/* jshint node:true */
module.exports = function( grunt ) {
	grunt.initConfig({
			pkg: grunt.file.readJSON('package.json'),

		// Compile all .scss files.
		sass: {
			dist: {
				options: {
					style: 'expanded',
					require: 'susy'
				},
				files: {
					'custom/style.css': 'custom-dev/*.scss'
				}
			}
		},

		// Minify all .css files.
		cssmin: {
			main: {
				files: [{
					expand: true,
					cwd: 'custom/',
					src: ['*.css'],
					dest: 'custom',
					ext: '.css'
				}]
			}
		},

		// Watch changes for assets.
		watch: {
			sass: {
				files: 'custom-dev/*.scss',
				tasks: [ 'sass', 'cssmin']
			}
		}

	});


	// Load NPM tasks to be used here
	// grunt.loadNpmTasks( 'grunt-contrib-jshint' );
	// grunt.loadNpmTasks( 'grunt-contrib-uglify' );
	grunt.loadNpmTasks( 'grunt-contrib-sass');
	grunt.loadNpmTasks( 'grunt-contrib-cssmin' );
	grunt.loadNpmTasks( 'grunt-contrib-watch' );
	// grunt.loadNpmTasks( 'grunt-wp-i18n' );
	// grunt.loadNpmTasks( 'grunt-checktextdomain' );
	// grunt.loadNpmTasks( 'grunt-contrib-copy' );
	// grunt.loadNpmTasks( 'grunt-rtlcss' );
	// grunt.loadNpmTasks( 'grunt-postcss' );
	// grunt.loadNpmTasks( 'grunt-contrib-compress' );
	// grunt.loadNpmTasks( 'grunt-stylelint' );

	grunt.registerTask('default', ["sass"]);

};
