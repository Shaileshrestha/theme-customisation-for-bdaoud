<?php
/**
 * Plugin Name:       Theme Customisations for bDaoud.com
 *
 * Description:       A handy little plugin to contain theme customisation snippets.
 * Plugin URI:        http://bDaoud.com
 * Version:           1.0.0
 * Authors:           WooThemes, Shailesh Shrestha
 * Author URI:        https://www.bDaoud.com/
 * Requires at least: 3.0.0
 * Tested up to:      4.4.2
 *
 * @package Theme_Customisations
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

/**
 * Main Theme_Customisations Class
 *
 * @class Theme_Customisations
 * @version	1.0.1
 * @since 1.0.0
 * @package	Theme_Customisations
 */
final class Theme_Customisations {

	/**
	 * Set up the plugin
	 */
	public function __construct() {
		add_action( 'init', array( $this, 'theme_customisations_setup' ), -1 );
		require_once( 'custom/functions.php' );
	}

	/**
	 * Setup all the things
	 * @see wc_get_template()
	 */
	public function theme_customisations_setup() {
		add_action( 'wp_enqueue_scripts', array( $this, 'theme_customisations_css' ), 999 );
		add_action( 'wp_enqueue_scripts', array( $this, 'theme_customisations_js' ) );
		add_filter( 'template_include',   array( $this, 'theme_customisations_template' ), 11 );
		add_filter( 'wc_get_template',    array( $this, 'theme_customisations_wc_get_template' ), 11, 5 );

		add_filter('wc_get_template_part', array($this, 'theme_customisations_wc_get_template_part'), 11, 3);
	}

	/**
	 * Enqueue the CSS
	 *
	 * @return void
	 */
	public function theme_customisations_css() {
		wp_enqueue_style( 'custom-css', plugins_url( '/custom/style.css', __FILE__ ) );
	}

	/**
	 * Enqueue the Javascript
	 *
	 * @return void
	 */
	public function theme_customisations_js() {
		wp_enqueue_script( 'custom-js', plugins_url( '/custom/custom.js', __FILE__ ), array( 'jquery' ) );

	}

	/**
	 * Look in this plugin for template files first.
	 * This works for the top level templates (IE single.php, page.php etc). However, it doesn't work for
	 * template parts yet (content.php, header.php etc).
	 *
	 * Relevant track ticket; https://core.trac.wordpress.org/ticket/13239
	 *
	 * @param  string $template template string.
	 * @return string $template new template string.
	 */
	public function theme_customisations_template( $template ) {
		if ( file_exists( untrailingslashit( plugin_dir_path( __FILE__ ) ) . '/custom/templates/' . basename( $template ) ) ) {
			$template = untrailingslashit( plugin_dir_path( __FILE__ ) ) . '/custom/templates/' . basename( $template );
		}

		return $template;
	}

	/**
	 * Look in this plugin for WooCommerce template overrides.
	 *
	 * For example, if you want to override woocommerce/templates/cart/cart.php, you
	 * can place the modified template in <plugindir>/custom/templates/cart/cart.php
	 *
	 * @param string $located is the currently located template, if any was found so far.
	 * @param string $template_name is the name of the template (ex: cart/cart.php).
	 * @return string $located is the newly located template if one was found, otherwise
	 *                         it is the previously found template.
	 */
	public function theme_customisations_wc_get_template( $located, $template_name, $args, $template_path, $default_path ) {
		$plugin_template_path = untrailingslashit( plugin_dir_path( __FILE__ ) ) . '/custom/templates/' . $template_name;

		if ( file_exists( $plugin_template_path ) ) {
			$located = $plugin_template_path;
		}

		return $located;
	}
	/**
	 * Look in this plugin for WooCommerce template-part overrides.
	 *
	 * For example, if you want to override woocommerce/templates/content-single-product.php, you
	 * can place the modified template in <plugindir>/custom/templates/woocommerce/content-single-product.php
	 * @param $template
	 * @param $slug
	 * @param $name
	 *
	 * @return string
	 */
	public function theme_customisations_wc_get_template_part( $template, $slug, $name) {
		$plugin_template_path = untrailingslashit( plugin_dir_path(__FILE__)).'/custom/templates/' . "{$slug}-{$name}.php";
		if ( file_exists ($plugin_template_path) ) {
			$template = $plugin_template_path;
		}
		return $template;

	}

} // End Class


/**
 * The 'main' function
 *
 * @return void
 */
function theme_customisations_main() {
	new Theme_Customisations();
}

/**
 * Initialise the plugin
 */
add_action( 'plugins_loaded', 'theme_customisations_main' );
