<?php
/**
 * Functions.php
 *
 * @package  Theme_Customisations
 * @author   WooThemes, Shailesh Shrestha
 * @since    1.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

/**
 * functions.php
 * Add PHP snippets here
 */


 /**
 	* Remove footer
 */
  add_action('init', 'bdaoud_remove_stuffs');
  function bdaoud_remove_stuffs(){
  	add_filter('is_active_sidebar', '__return_false');
  	remove_action( 'storefront_footer', 'storefront_credit', 20);
 }

 // Rearrange the Header elements

 add_action( 'init', 'rearrange_header', 15);
 function rearrange_header() {
 	/**
 	 * Functions hooked into storefront_header action BEFORE
 	 *
 	 * @hooked storefront_skip_links                       - 0
 	 * @hooked storefront_social_icons                     - 10
 	 * @hooked storefront_site_branding                    - 20
 	 * @hooked storefront_secondary_navigation             - 30 => Change to   - 0
 	 * @hooked storefront_product_search                   - 40 => Changed to  - 30
 	 * @hooked storefront_primary_navigation_wrapper       - 42
 	 * @hooked storefront_primary_navigation               - 50
 	 * @hooked storefront_header_cart                      - 60
 	 * @hooked storefront_primary_navigation_wrapper_close - 68
	*/
 	remove_action( 'storefront_header', 'storefront_secondary_navigation',             30 );
 	add_action( 'storefront_header', 'storefront_secondary_navigation',             0 );
 	remove_action( 'storefront_header', 'storefront_product_search', 40);
 	add_action('storefront_header', 'storefront_product_search',    30);
	remove_action( 'storefront_header', 'storefront_header_cart', 60);
	add_action( 'storefront_header', 'storefront_header_cart', 40);
 	// remove_action('storefront_header', 'storefront_primary_navigation', 50);
 	// remove_action('storefront_header', 'storefront_primary_navigation_wrapper', 42);
 	// remove_action('storefront_header', 'storefront_primary_navigation_wrapper_close', 68);
	add_action('storefront_header', 'bdaoud_shipping_country_info', 15);
 }

 function bdaoud_shipping_country_info() {
	 /**
	 * Site country wrapper and readline_redisplay
	 *
	 *
	 * @since 1.0.0
	 * @return void
	 */
	 $country = '';
    if (isset($_GET['country']) && '' != $_GET['country'] && wcj_is_user_role('administrator')) {
    	$country = $_GET['country'];
    } else {
        // Get the country by IP
        $location = WC_Geolocation::geolocate_ip();
        // Base fallback
        if (empty($location['country'])) {
            $location = wc_format_country_state_string(apply_filters('woocommerce_customer_default_location', get_option('woocommerce_default_country')));
        }
        $country = isset($location['country']) ? $location['country'] : '';
    }
	?>
	<div class="site-country">SHIPPING TO: <span class="country-name"> <?php echo  esc_html(WC()->countries->countries[$country]); ?></span>
	</div>
	<?php
}
